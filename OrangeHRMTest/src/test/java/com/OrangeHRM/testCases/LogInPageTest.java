package com.OrangeHRM.testCases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.OrangeHRM.qa.base.TestBase;
import com.OrangeHRM.qa.pages.LogInPage;

public class LogInPageTest extends TestBase {
	LogInPage loginpage;

	public LogInPageTest() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		initialization();
		loginpage = new LogInPage();
	}

	@Test(priority = 1)
	public void logInPageTitleTest() {

		String title = loginpage.validatePageTitle();
		Assert.assertEquals(title, "OrangeHRM");
	}

	@Test(priority = 2)
	public void logInTest() throws InterruptedException {
		loginpage.login();
		Assert.assertTrue(loginpage.validateLogin());
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}
}
