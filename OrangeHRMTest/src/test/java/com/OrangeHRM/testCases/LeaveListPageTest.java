package com.OrangeHRM.testCases;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.OrangeHRM.qa.base.TestBase;
import com.OrangeHRM.qa.pages.DashboardPage;
import com.OrangeHRM.qa.pages.LeaveListPage;
import com.OrangeHRM.qa.pages.LogInPage;

public class LeaveListPageTest extends TestBase{

	LogInPage loginpage;
	DashboardPage dashbordPage;
	LeaveListPage leaveListpage;
	
	String fromDate = "2020-10-01";
	String toDate= "2021-10-01";
	
	
	
	public LeaveListPageTest() {
		super();
	}
	
	
	@BeforeTest
	public void Setup() {
		initialization();
		loginpage = new LogInPage();
		loginpage.login();
		dashbordPage.clickOnLeaveListPage();
	}
	
	@Test(priority=1)
	public void searchEmpLeaveListTest(String From_date, String To_Date, String Employee) {
		leaveListpage.searchEmpLeaveList(fromDate, toDate, LeaveListPage.empName);
		
	}
	
	
	@Test(priority=2)
	public void verifyEmployeeInLeaveListPageTest() {
		
		Assert.assertEquals(leaveListpage.verifyLeave(), LeaveListPage.empName, 
				"Leave is not assigned to employee  ::"+LeaveListPage.empName);
	}
	
	
	@Test(priority=3)
	public void cancelLeaveTest() {
		leaveListpage.cancelLeave();
	}
	
	@Test(priority=4)
	public void verifyLeaveCancelTest() {
	
		Assert.assertTrue(leaveListpage.verifyLeaveCancel().contains("Cancelled"));
	}
	
	@Test(priority=5)
	public void verifyLogoutTest() {
		leaveListpage.logOut();
		
		//Verify login page LOGO after clicking on Logout
		Assert.assertTrue(leaveListpage.logOut());
	}
	
	@AfterTest
	public void teardown() {
		driver.quit();
	}
	
	
}
