package com.OrangeHRM.testCases;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.OrangeHRM.qa.base.TestBase;
import com.OrangeHRM.qa.pages.AssignLeavePage;
import com.OrangeHRM.qa.pages.DashboardPage;
import com.OrangeHRM.qa.pages.LogInPage;
import com.OrangeHRM.qa.util.TestUtil;

public class AssignLeavePageTest extends TestBase {
    
	LogInPage loginpage;
	DashboardPage dashbordPage;
	AssignLeavePage assignLeavePage;
	
	
	String SheetName = "AssignLeave";
	
	public AssignLeavePageTest() {
		super();
	}
	
	
	@BeforeTest
	public void setUp() throws InterruptedException {
		initialization();
		loginpage = new LogInPage();
		loginpage.login();
		dashbordPage.clickOnAssignLeaveLink();
	}
	
	
	@DataProvider
	public String[][] getAssignLeaveTestData() {
		String data[][] = TestUtil.readTestData(SheetName);
		return data;
	}
	
	@Test(dataProvider="getAssignLeaveTestData")
	public void validateAssignLeave(String Employee_Name, String Leave_Type, String From_Date, 
			String To_Date, String Partial_Days, String Comment) {
		
		assignLeavePage.assignLeave(Employee_Name, Leave_Type, From_Date, To_Date, Partial_Days, Comment);
		
		//Verify whether Leave is assigned
		Assert.assertTrue(assignLeavePage.validateAssignLeave());
	}
	
	
	
	
	@AfterTest
	public void teardown() {
		driver.quit();
	}
	
}
