package com.OrangeHRM.testCases;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.OrangeHRM.qa.base.TestBase;
import com.OrangeHRM.qa.pages.AddEmployeePage;
import com.OrangeHRM.qa.pages.DashboardPage;
import com.OrangeHRM.qa.pages.LogInPage;
import com.OrangeHRM.qa.util.TestUtil;

public class AddEmployeePageTest extends TestBase {

	AddEmployeePage addEmpPage;
	LogInPage loginpage;
	DashboardPage dashbordPage;

	String sheetName = "AddEmployeeTestData";

	public AddEmployeePageTest() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		initialization();
		loginpage = new LogInPage();
		loginpage.login();
		dashbordPage.clickOnAddEmployeeLink();

	}

	@DataProvider
	public String[][] getEmployeeTestData() {
		String data[][] = TestUtil.readTestData(sheetName);
		return data;
	}

	@Test(dataProvider = "getEmployeeTestData")
	public void validateCreateNewEmployee(String first_Name, String last_Name, String emp_ID, String other_ID,
			String driver_lic_nmbr, String license_exp_date, String gender, String marital_status,
			String Date_of_birth) {

		
		addEmpPage.createNewEmployee(first_Name, last_Name, emp_ID, other_ID, driver_lic_nmbr, license_exp_date, gender,marital_status, Date_of_birth);
		
		//Verify whether New Employee is added
		Assert.assertTrue(addEmpPage.validateAddEmployee());

	}

	@AfterTest
	public void teardown() {
		driver.quit();
	}
}
