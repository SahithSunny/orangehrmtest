package com.OrangeHRM.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.OrangeHRM.qa.base.TestBase;
import com.OrangeHRM.qa.util.TestUtil;

public class LogInPage extends TestBase{

	// PageFactory for LogIn Page
	@FindBy (xpath="//input[@id='txtUsername']")
	WebElement username;
	
	@FindBy (xpath="//input[@id='txtPassword']")
	WebElement password;
	
	@FindBy (xpath="//input[@id='btnLogin']")
	WebElement logInBtn;
	
	
public LogInPage() {
		
		// Initializing the page Objects. 
		PageFactory.initElements(driver, this); 	
}

//Actions
public String validatePageTitle() {
	return driver.getTitle();
}

public DashboardPage login() {
	String[][] data = TestUtil.readTestData("LogInTestData");
	String userName = data[0][0].toString();
	String passWord=  data[0][1].toString();
	
	username.sendKeys(userName);
	password.sendKeys(passWord);
	logInBtn.click();
	
	return new DashboardPage();
	
}

public boolean validateLogin() {
	WebElement WelcomeUsername= driver.findElement(By.xpath("//a[contains(text(),'Welcome Lindark')]"));
	return WelcomeUsername.isDisplayed();
	
}
	
}
