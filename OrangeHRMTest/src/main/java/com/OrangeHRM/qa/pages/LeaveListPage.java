package com.OrangeHRM.qa.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.OrangeHRM.qa.base.TestBase;

public class LeaveListPage extends TestBase {

	public static String empName = "Sahith Sunny";
	
	

	// PageFactory in Leave list page
	@FindBy(xpath = "//input[@id='calFromDate']")
	WebElement From;

	@FindBy(xpath = "//input[@id='calToDate']")
	WebElement To;

	@FindBy(xpath = "//div[@id='leaveList_chkSearchFilter_checkboxgroup']/label[1]/following-sibling::input[1]")
	WebElement leaveStatus;

	@FindBy(xpath = "//input[@id='leaveList_txtEmployee_empName']")
	WebElement employee;

	@FindBy(xpath = "//input[@id='btnSearch']")
	WebElement search;

	@FindBy(xpath="//a[contains(text(),'Welcome Linda')]")
	WebElement profileDropdown;
	
	@FindBy(xpath="//a[contains(text(),'Logout')]")
	WebElement logOut;
	
	@FindBy(xpath="//div[@id='divLogo']/img")
	WebElement logInLOGO;
	
	
	public void searchEmpLeaveList(String From_date, String To_Date, String Employee) {
		From.clear();
		From.sendKeys(From_date);
		
		To.clear();
		To.sendKeys(To_Date);
		
		leaveStatus.click();
		employee.sendKeys(Employee);
		search.click();
	}
	
	
	public String verifyLeave() {

		List<WebElement> names = driver.findElements(By.xpath("//tbody/tr/td[2]/a"));
		for (int i = 0; i < names.size(); i++) {

			String name = names.get(i).getText();

			if (name.contains(empName)) {
				return name;
			}
		}
		return null;
	}
	
	
	public void cancelLeave() {
		WebElement actionSelect = driver.findElement(By.xpath("//select[contains(@id, 'select_leave_action')]"));
		new Select(actionSelect).selectByVisibleText("Cancel");
		
		driver.findElement(By.xpath("//input[(@id='btnSave')]")).click();
	}
	
	public String verifyLeaveCancel() {
		WebElement leaveStatus = driver.findElement(By.xpath("//tbody/tr/td[2]/following-sibling::td[4]/a"));
		String status = leaveStatus.getText();
		return status;
	}
	
	
	public boolean logOut() {
		
		profileDropdown.click();
		logOut.click();
		
		return (logInLOGO.isDisplayed());
		
	}
	

}
