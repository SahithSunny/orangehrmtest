package com.OrangeHRM.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.OrangeHRM.qa.base.TestBase;

public class AssignLeavePage extends TestBase{
	
	//PageFactory of Assign Leave Page
	
	@FindBy(xpath="//input[@id='assignleave_txtEmployee_empName']")
	WebElement empName;
	
	@FindBy(xpath="//input[@id='assignleave_txtFromDate']")
	WebElement fromDate;
	
	@FindBy(xpath="//input[@id='assignleave_txtToDate']")
	WebElement toDate;
	
	@FindBy(xpath="//textarea[@id='assignleave_txtComment']")
	WebElement comment;
	
	@FindBy(xpath="//input[@id='assignBtn']")
	WebElement assignBtn;
	
	
	

	
	public void assignLeave(String employee_Name, String leave_type, String from_Date, String to_Date, 
			String partial_Days, String Comment) {
		empName.sendKeys(employee_Name);
		empName.sendKeys(Keys.ENTER);
		
		Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='assignleave_txtLeaveType']")));
		dropdown.selectByVisibleText(leave_type);
		
		fromDate.click();
		fromDate.sendKeys(from_Date);
		
		toDate.clear();
		toDate.sendKeys(to_Date);
		
		dropdown = new Select(driver.findElement(By.xpath("//select[@id='assignleave_partialDays']")));
		dropdown.selectByVisibleText(partial_Days);
		
		comment.sendKeys(Comment);
		assignBtn.click();
		
		//Click on the Apply leave confirmation OK button.
		driver.findElement(By.xpath("//input[@id='confirmOkButton']")).click();
		
	}
	
	public boolean validateAssignLeave() {
		WebElement assignLeaveSucess = driver.findElement(
				By.xpath("//*[contains(text(),'Successfully Assigned')]"));
		return assignLeaveSucess.isDisplayed();
	}
	
}
