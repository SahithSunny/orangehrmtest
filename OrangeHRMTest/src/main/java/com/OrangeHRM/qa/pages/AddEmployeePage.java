package com.OrangeHRM.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.OrangeHRM.qa.base.TestBase;

public class AddEmployeePage extends TestBase {

	// PageFactory for Add Employee Page

	@FindBy(xpath = "//input[@id='firstName']")
	WebElement firstName;

	@FindBy(xpath = "//input[@id='lastName']")
	WebElement lastName;

	@FindBy(xpath = "//input[@id='employeeId']")
	WebElement employeeID;

	@FindBy(xpath = "//input[@value='Save'][@id='btnSave']")
	WebElement saveBtn;

	@FindBy(xpath = "//input[@value='Edit'][@id='btnSave']")
	WebElement editBtn;

	@FindBy(xpath = "//input[@id='personal_txtOtherID']")
	WebElement otherID;

	@FindBy(xpath = "//input[@id='personal_txtLicenNo']")
	WebElement driverLicenseNmbr;

	@FindBy(xpath = "//input[@id='personal_txtLicExpDate']")
	WebElement licenseExpDate;

	@FindBy(xpath = "//input[@id='personal_DOB']")
	WebElement dateOfBirth;

	
	
	
	
	
	public void createNewEmployee(String ftName, String ltName, String empID, String othrID, String driverLicNmbr,
			String licExpDt, String gndr, String mrtlStatus, String DOB) {

		firstName.sendKeys(ftName);
		lastName.sendKeys(ltName);
		employeeID.sendKeys(empID);
		saveBtn.click();

		editBtn.click();
		otherID.sendKeys(othrID);
		driverLicenseNmbr.sendKeys(driverLicNmbr);

		licenseExpDate.click();
		licenseExpDate.sendKeys(licExpDt);

		driver.findElement(By.xpath("//label[contains(text(),'" + gndr + "')]")).click();
		Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='personal_cmbMarital']")));
		dropdown.selectByVisibleText(mrtlStatus);

		dateOfBirth.click();
		dateOfBirth.sendKeys(DOB);
		saveBtn.click();

	}
	
	

	public boolean validateAddEmployee() {
		WebElement addEmployeSucess = driver.findElement(
				By.xpath("//div[contains(text(),'Successfully Saved')][@class='message success fadable']"));
		return addEmployeSucess.isDisplayed();
	}

}
