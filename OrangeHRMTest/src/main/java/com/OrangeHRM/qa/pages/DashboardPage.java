package com.OrangeHRM.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.OrangeHRM.qa.base.TestBase;

public class DashboardPage extends TestBase {
	
	Actions act;

	@FindBy(xpath="//a[@id='menu_pim_viewPimModule']/b")
	WebElement PIM;
	
	@FindBy(xpath="//a[text()='Add Employee']")
	WebElement addEmployee;
	
	
	@FindBy(xpath="//b[text()='Leave']")
	WebElement leave;
	
	@FindBy(xpath="//a[@id='menu_leave_assignLeave']")
	WebElement assignLeave;
	
	@FindBy(xpath="//a[contains(text(),'Leave List')]")
	WebElement leaveList;

	
	
	public AddEmployeePage clickOnAddEmployeeLink() {
		act = new Actions(driver);
		act.moveToElement(PIM).build().perform();
		addEmployee.click();
		return new AddEmployeePage();
	}
	
	
	public AssignLeavePage clickOnAssignLeaveLink() {
		act = new Actions(driver);
		act.moveToElement(leave).build().perform();
		assignLeave.click();
		return new AssignLeavePage();
	}
	
	
	public LeaveListPage clickOnLeaveListPage() {
		 act = new Actions(driver);
		 act.moveToElement(leave).build().perform();
		 leaveList.click();
		 return new LeaveListPage();
		
	}
	
}
