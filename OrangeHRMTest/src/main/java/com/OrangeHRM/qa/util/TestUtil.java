package com.OrangeHRM.qa.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class TestUtil {

		public static long PAGE_LOAD_TIMEOUT = 15;
		public static long IMPLICIT_WAIT = 10;
		
		// TestData Excel Sheet Location
		public static String TESTDATA_SHEET_PATH =System.getProperty("user.dir")+ 
				"/src/main/java/com/OrangeHRM/qa/testData/OrangeHRM_TestData.xlsx";
	
		static Workbook book;
		static Sheet sheet;
		

		
		public static String[][] readTestData(String sheetName) {
			FileInputStream file = null;
			try {
				file = new FileInputStream(TESTDATA_SHEET_PATH);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			try {
				book = WorkbookFactory.create(file);
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			sheet = book.getSheet(sheetName);
			String[][] data = new String[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
			
			for (int i = 0; i < sheet.getLastRowNum(); i++) {
				for (int k = 0; k < sheet.getRow(0).getLastCellNum(); k++) {
					
					//Fetching the data from 'i+1' row, as the first row has Column heading
					data[i][k] = sheet.getRow(i + 1).getCell(k).toString();
					// System.out.println(data[i][k]);
				}
			}
			return data;
		}
	
}
